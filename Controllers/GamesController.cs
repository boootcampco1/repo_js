﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RawGameApi.Models;

namespace RawGameApi.Controllers
{
  [ApiController]
  [Route("api/v1/games")]
  public class GamesController : ControllerBase
  {
    private static List<Game> games = new List<Game> {
      new Game {Id = Guid.Parse("ab908249-7cf7-49cf-890e-d1c71cc08d59"), Name = "Space Invaders II", Genre = "Arcade", Platforms = new string[] {"PSP", "ATARI", "NINTENDO"}},
      new Game {Id = Guid.Parse("f61e14e8-be77-4275-b764-57151f721381"), Name = "Mario Bros", Genre = "Platform", Platforms = new string[] {"NINTENDO"}},
      new Game {Id = Guid.Parse("88670aaf-bba4-4d87-8416-30456d5438c7"), Name = "Uncharted IV", Genre = "Adventure", Platforms = new string[] {"PS4"}},
    };
    private readonly ILogger<GamesController> _logger;

    public GamesController(ILogger<GamesController> logger)
    {
      _logger = logger;
    }

    [HttpGet]
    public IEnumerable<Game> GetAllGames()
    {
      return games;
    }

    [HttpGet("{id}")]
    public Game GetGameById(Guid id)
    {
      return games.FirstOrDefault(game => game.Id == id);
    }

    [HttpPost]
    public Game CreateGame(Game game)
    {
      game.Id = Guid.NewGuid();
      games.Add(game);
      return game;
    }

    [HttpPut("{id}")]
    public void UpdateGame(Guid id, Game game)
    {
      var gameToUpdate = games.FirstOrDefault(game => game.Id == id);
      if (gameToUpdate != null)
      {
        games.Remove(gameToUpdate);
        games.Add(game);
      }
    }

    [HttpDelete("{id}")]
    public void DeleteGame(Guid id)
    {
      var gameToDelete = games.FirstOrDefault(game => game.Id == id);
      if (gameToDelete != null)
      {
        games.Remove(gameToDelete);
      }
    }
  }
}
